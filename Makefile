EXEC_DOCKER_COMPOSE = docker-compose
EXEC_SYMFONY = symfony

.PHONY = help start stop

.DEFAULT_GOAL := help

##

help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##

start: ## Start dev env
	@$(EXEC_DOCKER_COMPOSE) up -d \
	&& $(EXEC_SYMFONY) run -d yarn watch \
	&& $(EXEC_SYMFONY) serve -d

stop: ## Stop dev env
	@$(EXEC_SYMFONY) server:stop \
	&& $(EXEC_DOCKER_COMPOSE) stop

##
