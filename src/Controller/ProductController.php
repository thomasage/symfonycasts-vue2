<?php
declare(strict_types=1);

namespace App\Controller;

use ApiPlatform\Core\Api\IriConverterInterface;
use App\Entity\Category;
use App\Entity\Product;
use App\Repository\CategoryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

final class ProductController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage")
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render(
            'product/index.html.twig',
            [
                'categories' => $categoryRepository->findAll(),
            ]
        );
    }

    /**
     * @Route("/category/{id}", name="app_category")
     * @param Category $category
     * @param IriConverterInterface $iriConverter
     * @param CategoryRepository $categoryRepository
     * @return Response
     */
    public function showCategory(
        Category $category,
        IriConverterInterface $iriConverter,
        CategoryRepository $categoryRepository
    ): Response {
        return $this->render(
            'product/index.html.twig',
            [
                'categories' => $categoryRepository->findAll(),
                'currentCategoryId' => $iriConverter->getIriFromItem($category),
            ]
        );
    }

    /**
     * @Route("/product/{id}", name="app_product")
     * @param Product $product
     * @param CategoryRepository $categoryRepository
     * @param IriConverterInterface $iriConverter
     * @return Response
     */
    public function showProduct(
        Product $product,
        CategoryRepository $categoryRepository,
        IriConverterInterface $iriConverter
    ): Response {
        return $this->render(
            'product/index.html.twig',
            [
                'categories' => $categoryRepository->findAll(),
                'currentProductId' => $iriConverter->getIriFromItem($product),
            ]
        );
    }
}
